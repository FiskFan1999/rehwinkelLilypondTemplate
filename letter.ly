\version "2.25.16"

#(set-default-paper-size "letter" )

\paper {
  left-margin = 0.40\in
  right-margin = 0.40\in
  top-margin = 0.40\in
  bottom-margin = 0.40\in
}

