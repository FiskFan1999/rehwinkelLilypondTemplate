\version "2.24.0"

%{
  Defines a new ornament which uses the
  caesura.straight glyph.

  Use like so:
  g4\shake
%}

#(set! default-script-alist
       (append default-script-alist
         (list
          `(shake
             . (
                 (script-stencil . (feta . ("caesura.straight" . "caesura.straight")))
                 ; any other properties
                  ; (toward-stem-shift-in-column . 1.0)
        (side-relative-direction . ,UP)
                    (toward-stem-shift . 0.2)
                 ; (X-offset . 0.3)
                 (avoid-slur . around)
                 ; (padding . -4)
                 ; (direction . ,UP)
                 )))))

shake = #(make-articulation 'shake)

% { \stemUp g'-\shake \stemDown g'-\shake }

\layout {
  \context {
    \Score
    scriptDefinitions = #default-script-alist
  }
}
