\version "2.25.13"

emhdate = #(strftime "%b %d, %Y" (localtime (current-time)))

wills-copyright = \markup {\italic \center-column { \vspace #0.5 
    \line { \with-url "https://ftp.williamrehwinkel.net/scores/early/" "William Rehwinkel" \emhdate }
    \line { \with-url "https://creativecommons.org/licenses/by/4.0/" "CC BY 4.0" }
  }}

quarterNoteValues = \markup\box \line {  \bold "|" "=" \smaller \note { 4 } #0.5 }
halfNoteValues = \markup\box \line {  \bold "|" "=" \smaller \note { 2 } #0.5 }
wholeNoteValues = \markup\box \line {  \bold "|" "=" \smaller \lower #-0.5 \note { 1 } #0 }

timesigBracket = #(lambda (grob) (bracketify-stencil (ly:time-signature::print grob) Y 0.15 0.2 0.1))

\layout { \context {
  \Voice \autoBeamOff
} 
}

#(define-markup-command 
   (library-signum-page layout props lib sig pag) 
   (string? string? string?) 
   (interpret-markup layout props
                     #{\markup \fontsize #-1.5 \line\typewriter {  \concat { $(string-append lib " ") \concat { \bold $sig $(if (string-null? pag) "" (string-append " " pag)) } } } #}) )
                     
%dottedBarlines = { }

%{
\relative c' {
  \sectionLabel \markup\box\library-signum-page "D-B" "sig here" "P. 15"
  c4 c c c c c c c
}
\relative c' {
  \sectionLabel \markup\box\library-signum-page "D-B" "sig here" ""
  c4 c c c c c c c
}
%}
