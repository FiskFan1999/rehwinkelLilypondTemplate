%{

William's lilypond stylesheet.

Uses cadence and gonville-bracket fonts, and Liberation family of fonts (from Libreoffice).

Introduces a new markup command \openBracket for drawing square brackets (see below for usage example)

Also changes the settings used for rendering:
- Modifies beam thickness and gap between beams.
- Uses french-beaming, allow breakable beams.
- Use Jean Abou Samra's beam stencil to whiteout stave lines underneath the beam to avoid collisions.
- Unbeamed notes have flat flags that look identical to beams
- Glissando is by default a thick straight line, used to connect notes that are tied for a long time.
- Modified slur rendering settings
- Modified stave spacing settings
- Parenthesize uses square brackets (for editorial additions)
- Use portrait architect-A paper size (9x12in) by default

Copyright 2022 William Rehwinkel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

%}
\version "2.25.13"

\pointAndClickOff

% markSig is used for a textEndMark which appears underneath the staff. This may be used for the composer's signature and date of composition which is customarily written underneath the final system of the peace. Should be called after the final note of the piece.
markSig = \once \tweak direction #DOWN \textEndMark \etc

% current date at time of compiling
date = #(strftime "%b %d, %Y" (localtime (current-time)))

% include dateSig after the last note of a piece to sign the piece
% and the date at time of compilation
dateSig = { \markSig \markup { "W.R." \date } }

#(set-default-paper-size "arch a" )

% organ bracket
% see https://lists.gnu.org/archive/html/lilypond-user/2017-03/msg00611.html

% note: for appearing after barline, use
% \once \override PianoStaff.BarLine.extra-spacing-width = #'(0 . 2)

% first argument: height
% second argument: negative offset (how many columns of accidentals are next to the note. Positive moves the bracket to the left to prevent collisions.

spaceAfterBar = {\once \override PianoStaff.BarLine.extra-spacing-width = #'(0 . 2)}

#(define-markup-command (openBracket layout props height offset) (number? number?)
  (interpret-markup layout props
    (markup #:line (#:with-dimensions (cons 3 0) (cons 0 0) 
      (#:path 0.10
        (list (list (quote moveto) (* offset -1) 0)
        (list (quote lineto) (- (* offset -1) 1) 0)
        (list (quote lineto) (- (* offset -1) 1) (* height -1))
        (list (quote lineto) (* offset -1) (* height -1))))))))

% for half open brackets, see https://mail.gnu.org/archive/html/lilypond-user/2022-06/msg00001.html

% square bracket parenthesize
% from https://lsr.di.unimi.it/LSR/Snippet?id=564
#(define-public (bracket-stencils grob)
  (let ((lp (grob-interpret-markup grob (markup #:fontsize 5.5 #:translate (cons -0.4 -0.5) "[")))
        (rp (grob-interpret-markup grob (markup #:fontsize 5.5 #:translate (cons -0.3 -0.5) "]"))))
    (list lp rp)))

% use cadence and gonville fonts
% use architect A size paper
\paper {
  % footnote settings
  footnote-separator-markup = \markup{\draw-dashed-line #'(40 . 0)} 
    %auto-first-page-number = ##t
    %blank-last-page-penalty = #10
    print-first-page-number = ##f
property-defaults.fonts.music = "cadence"
property-defaults.fonts.serif = "Liberation Serif"
property-defaults.fonts.sans = "Liberation Sans"
property-defaults.fonts.typewriter = "Liberation Mono"
}

\layout {
  \context {
    \PianoStaff
    \override InstrumentName.self-alignment-X = #RIGHT
  }
    \context{
        \Voice
        \override Accidental.hide-tied-accidental-after-break = ##t
        \remove Forbid_line_break_engraver
        \override NoteHead.style = #'altdefault
        \override Stem.french-beaming = ##t
        \override Flag.stencil = #(straight-flag 0.6 0.75 0 1.5 0 1.5) % Thank you to Jean Abou Samra from lilypond users mailing list
        \override Beam.beam-thickness = #0.6
        \override Beam.length-fraction = #1.0
        \override Beam.damping = #4
        \override Beam.breakable = ##t
        \override Glissando.thickness = 4
        \override Glissando.breakable = ##t
        \override Glissando.after-line-breaking = ##t
        \override Glissando.minimum-length = #5
        \override Glissando.minimum-length-after-break = #4
        \override Glissando.springs-and-rods = #ly:spanner::set-spacing-rods
        \override Slur.thickness = #2
        %\override Slur.height-limit = #1
        \override Tie.thickness = #2
        \override Tie.springs-and-rods = #ly:spanner::set-spacing-rods
        \override Tie.minimum-length = #4
        \override Tie.minimum-length-after-break = #4
        %\override Parentheses.font-size = #-1
        \override Parentheses.padding = #0.6
        \override Parentheses.stencils = #bracket-stencils
        \override ParenthesesItem.stencils = #bracket-stencils
    }
    \context {
        \Staff
        % starting in 2.16.x later versions, time signature font doesn't work anymore
        %\override TimeSignature.fonts.music = "emmentaler"
        \override VerticalAxisGroup.staff-staff-spacing.basic-distance = #0
        \override VerticalAxisGroup.staff-staff-spacing.padding = #1
    }
    \context {
        \Score
        \override MetronomeMark.flag-style = #'flat-flag
        \override SystemStartBrace.fonts.music = "emmentaler"
    }
    \context {
      \FiguredBass
      \override BassFigure.fonts.music = "emmentaler"
      \override BassFigure.font-size = 1.8
    }
  #(use-modules (ice-9 match)
                (srfi srfi-1))
  \context {
  % This beam whiteout stencil is written by Jean Abou Samra
  % Temporary solution to whiteout stave lines inbetween beams
  % To avoid collision
    \Score
    \override Beam.stencil =
      #(grob-transformer
        'stencil
        (lambda (grob orig)
          (define (sanitize lst)
            (filter (match-lambda ((x . y) (and (finite? x) (finite? y))))
                    lst))
          (match-let* (((down . up) (ly:skylines-for-stencil orig X))
                       (down-points (sanitize (ly:skyline->points down X)))
                       (up-points (sanitize (ly:skyline->points up X))))
            (ly:stencil-add
             (stencil-with-color
              (ly:round-polygon
               (append-reverse down-points up-points)
               0.0)
              "white")
             orig))))
    \override Stem.layer = 10
  }
}

% Source: https://lists.gnu.org/archive/html/lilypond-user/2023-04/msg00246.html
#(define (bracketify-rest thick protrusion vpadding hpadding)
   (grob-transformer
    'stencil
    (lambda (grob stil)
      (let* ((dot (ly:grob-object grob 'dot #f))
             (ext-grobs (ly:grob-list->grob-array (if dot (list dot grob) (list grob))))
             (ref (ly:grob-common-refpoint-of-array grob ext-grobs X))
             (x-ext (coord-translate (ly:relative-group-extent ext-grobs ref X)
                                     (- (ly:grob-relative-coordinate grob ref X))))
             (ext (interval-widen (ly:stencil-extent stil Y)
                                  hpadding))
             (lb (ly:stencil-aligned-to (ly:bracket Y ext thick protrusion)
                                        X RIGHT))
             (rb (ly:stencil-aligned-to (ly:bracket Y ext thick (- protrusion))
                                        X LEFT)))
        (ly:stencil-add
         stil
         (ly:stencil-translate-axis rb (interval-end x-ext) X)
         (ly:stencil-translate-axis lb (interval-start x-ext) X))))))

#(define (wills-bracketify-stencil stil axis thick protrusion vpadding hpadding)
   ; Slightly modified version of bracketify-stencil procedure
   ; from standard library which adds horizontal padding
  "Add brackets around @var{stil}, producing a new stencil."

  (let* ((ext (ly:stencil-extent stil axis))
         ; (lb (ly:bracket axis ext thick protrusion))
         ; (rb (ly:bracket axis ext thick (- protrusion))))
         (lb (ly:bracket axis (cons (- (car ext) hpadding) (+ (cdr ext) hpadding)) thick protrusion))
         (rb (ly:bracket axis (cons (- (car ext) hpadding) (+ (cdr ext) hpadding)) thick (- protrusion))))
    (set! stil
          (ly:stencil-combine-at-edge stil (other-axis axis) 1 rb vpadding))
    (set! stil
          (ly:stencil-combine-at-edge stil (other-axis axis) -1 lb vpadding))
    stil))

bracketRest = { \override Rest.stencil = #(bracketify-rest 0.15 0.5 0.2 0.6)}
bracketmmRest = { \override MultiMeasureRest.stencil = #(lambda (grob) (wills-bracketify-stencil  (ly:multi-measure-rest::print grob) Y 0.15 0.5 0.2 0.6))}
