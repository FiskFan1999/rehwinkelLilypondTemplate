\version "2.25.16"

#(define (make-beckmann-thick-bar-line is-span grob extent)
  "Draw a thick bar line."
  (let* ((line-thickness-mod 1.6)
         (line-thickness (* line-thickness-mod (layout-line-thickness grob)))
         (thickness (* (ly:grob-property grob 'thick-thickness 1)
                       line-thickness))
         (extent-r (bar-line::widen-bar-extent-on-span grob extent))
         (extend-width 0.1)
         (extent (cons (- (car extent-r) extend-width) (+ (cdr extent-r) extend-width))))
    (ly:stencil-in-color (bar-line::draw-filled-box
     (cons 0 thickness)
     extent
     thickness
     extent
     grob) 1 1 1 )))

#(add-bar-glyph-print-procedure "b" make-beckmann-thick-bar-line)

\defineBarLine "b" #'( #f "b" "b" )

% to use, you simply add \bar "b" before the first note of the piece, like so

%{
<<
\new PianoStaff <<
  \new Staff \relative c' {
  \bar "b" c4 d e f | g a b c \bar "b" d4 e f g
}
\new Staff \relative c {\clef bass c2 c e e g g}
>>
\new Staff \relative c, {\clef bass c1 f bes}
>>
%}
