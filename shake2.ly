\version "2.25.2"

% William's second shake ornament
% by William Rehwinkel

%{
  Unlike the first shake ornament, this
  modifies the stencil used to draw the
  beam, so that the two line figure
  overlays the beam. This has several
  advantages, such as being more alike
  how this ornament was drawn in early
  music, and being less ambiguous with
  different voices.

  Please note that you have to call
  this ornament differently than the
  shake ornament. You have to specify
  it BEFORE the note (like the \xnote
  command) like so.

  \shakeNext g4

  Note that the command only affects
  the next note and not the ones
  after (like using \once)

  KNOWN ISSUES:
  This ornament does NOT work with
  whole notes, consider using the
  previous shake ornament instead.

  If you have more than one note in the same voice,,
  the ornament will not be in the right place
  and will overlap some notes.
  %}

#(define stemshake
   (grob-transformer
     'stencil
     (lambda (grob stil)
     (let* ([extents (ly:stencil-extent stil 1)]
            [direction (ly:grob-property grob 'direction 1)]
            [offset 1.1]
            [middle-y (if (= direction 1) (+ (car extents) offset) (- (cdr extents) offset))]
            [y-slant 0.25]
            [seperation 0.14])
     (ly:stencil-add
     stil
     (ly:line-interface::line grob -0.75 (+ (- middle-y y-slant) seperation) 0.75 (+ (+ middle-y y-slant) seperation))
     (ly:line-interface::line grob -0.75 (- (- middle-y y-slant) seperation) 0.75 (- (+ middle-y y-slant) seperation)))))))

shakeNext = \once \override Stem.stencil = #stemshake
